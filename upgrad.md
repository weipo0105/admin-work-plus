### 2021-11-23(2.0.1)

- 重要升级：把 vaw-layouts-x 项目合并到 Vue Admin Work X 中
- 重要升级：基本上组件都由Vue3重写，Javascript代码更换成Typescript代码
- 重要升级：重写部分组件样式，重构LayoutStore功能

### 2021-11-17(2.0.0)

- 重要升级：把 vaw-layouts-x 项目合并到 Vue Admin Work X 中

export interface RootState {
  root: string;
}

export interface AppState {
  version: string;
  appName: string;
}

export interface UserState {
  userOid: number;
  token: string;
  roleId: number;
  roleOid: number;
  roles: string[] | null;
  userName: string;
  nickName: string;
  avatar: string;
}

export interface MenuConfig {
  menuOid: number;
  token: string;
  parentOid: number;
  roles: string[] | null;
  menuName: string;
  menuIcon: string;
  menuUrl: string;
  operation: string;
  children: string[];
}

export interface ParentState {
  root: RootState;
  user: UserState;
  app: AppState;
}

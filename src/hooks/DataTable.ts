import { useLayoutStore } from "@/layouts";
import { reactive, ref, shallowReactive } from "vue";

export default function (): Record<string, any> {
  const dataList = shallowReactive([]) as Array<any>;
  const selectRows = shallowReactive([]) as Array<any>;
  const layoutStore = useLayoutStore()
  const tableConfig = reactive({
    stripe: layoutStore.state.theme !== 'dark',
    border: false,
    multiSelect: true, // 多选
    size: 'small',
    index: true,
    selectChange: (selection: any[]) => handleSelectionChange(selection),
    headerCellStyle: layoutStore.state.theme === 'dark' ? {
      color: '#ffffff'
    } : {
      backgroundColor: 'rgb(255, 255, 255)',
      color: '#333333'
    },
    height: '100%'
  });
  const tableLoading = ref(true);
  const handleSuccess = ({ data = [] }) => {
    dataList.length = 0
    dataList.push(...data)
    tableLoading.value = false
    return Promise.resolve({ data})
  }
  const handleSelectionChange = (tempSelectRows: Array<any>) => {
    selectRows.length = 0
    selectRows.push(...tempSelectRows)
  }
  const pageChanged = () => {
    doRefresh()
  }
  const doRefresh = () => {
    throw new Error('you must overwrite `doRefresh` function in `component`')
  }
  return {
    dataList,
    selectRows,
    tableConfig,
    tableLoading,
    handleSuccess,
    handleSelectionChange,
    pageChanged,
    doRefresh,
  }
}

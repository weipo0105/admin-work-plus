import Dialog from './common/Dialog.vue'
import BaseForm from './common/BaseForm.vue'
import TableHeader from './table/TableHeader.vue'
import TableFooter from './table/TableFooter.vue'
import { ElMessage, ElMessageBox, MessageBoxData } from 'element-plus';


export interface DialogConfig {
  beforeShowAction?: () => void;
  autoClose?: boolean;
  innerTitle?: string;
  showSubmitLoading?: boolean;
  validator?: () => boolean;
}

export interface ObjData {
  [key: string]: any;
}

export type DialogType = InstanceType<typeof Dialog>;

export type BaseFormType = InstanceType<typeof BaseForm>;

export type TableHeader = InstanceType<typeof TableHeader>;

export type TableFooter = InstanceType<typeof TableFooter>;

export function showConfirmBox(message:string, title: string, obj: any): Promise<MessageBoxData> {
  return ElMessageBox.confirm(message, title,obj)
}

export function showSuccessMessage(message = ''): void {
  ElMessage.success({ message })
}

export function showErrorMessage(message = ''): void {
  ElMessage.error({ message })
}

export function showInfoMessage(message = ''): void {
  ElMessage.info({ message })
}